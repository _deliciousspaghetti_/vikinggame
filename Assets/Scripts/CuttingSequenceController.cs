﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CuttingSequenceController : MonoBehaviour, IUtencil
{
    public float horizontalInput { get; set; }
    public float verticalInput { get; set; }
    public Action onTapAction { get; set; }
    public bool isDragging { get; set; }

    [SerializeField] private Transform knifeTransform;
    [SerializeField] private Transform objectToCutTransform;
    [Space]
    [SerializeField] private float     moveDuration                              = 0.1f;
    [SerializeField] private Ease      easeType1                                 = Ease.InOutCirc;
    [SerializeField] private Ease      easeType2                                 = Ease.OutCirc;
    [SerializeField] private float     cuttingPointOffset                        = .01f;
    [SerializeField] private bool      setOffsetAsPositionInsteadOfAddToPosition = true;
    [Header("Cutting Piece Parameters")]
    [SerializeField] private float     explosionPower                            = 1f;
    [SerializeField] private float     explosionRadius                           = .3f;
    [SerializeField] private float     explosionVerticalOffset                   = .03f;
    [SerializeField] private float     moveAwayForce                             = 1f;

    private bool hasCompletedPreviousStep = false;
    private Rigidbody[] piecesToCutBodies;
    private List<Vector3> cuttingPoints;
    private int nextCuttingPointIndex = 0;
    private int nextPieceIndex = 0;

    private void Awake()
    {

    }

    private void OnEnable()
    {
        piecesToCutBodies = objectToCutTransform.GetComponentsInChildren<Rigidbody>();

        // evaluating points for knife to cut at
        cuttingPoints = new List<Vector3>();
        for(int i = 0; i < piecesToCutBodies.Length; i++)
        {
            if(i + 1 < piecesToCutBodies.Length)
            {
                cuttingPoints.Add(new Vector3(piecesToCutBodies[i].transform.position.x +
                    (piecesToCutBodies[i + 1].transform.position.x - piecesToCutBodies[i].transform.position.x) / 2,
                    piecesToCutBodies[i].transform.position.y,
                    piecesToCutBodies[i].transform.position.z));
            }
        }

        var gm = FindObjectOfType<GameManager>();
        gm.currentUtencil = this;
        gm.playerInput.uc = this;
        onTapAction += this.handleInput;

        if(cuttingPoints.Count != 0)
        {
            initializeKnifeController();
        }
        else
        {
            Debug.Log("Nothing to cut.");
        }
        Debug.Log($"currentUtencil={gameObject.name}");
    }

    private void OnDisable()
    {
        onTapAction -= this.handleInput;
    }

    private void Start()
    {
    }

    private void initializeKnifeController()
    {
        var knifeOffsetPosition = knifeTransform.parent.InverseTransformPoint(cuttingPoints[0]);
        knifeOffsetPosition.y = setOffsetAsPositionInsteadOfAddToPosition ? cuttingPointOffset : knifeOffsetPosition.y + cuttingPointOffset;
        knifeTransform.localPosition = knifeOffsetPosition;

        nextCuttingPointIndex = 0;
        nextPieceIndex = 0;
        hasCompletedPreviousStep = true;
    }

    public void handleInput()
    {
        if(knifeTransform == null || objectToCutTransform == null || !hasCompletedPreviousStep || cuttingPoints == null)
            return;

        if(cuttingPoints.Count == 0)
            return;

        hasCompletedPreviousStep = false;
        knifeTransform.DOLocalMove(knifeTransform.parent.InverseTransformPoint(cuttingPoints[nextCuttingPointIndex]), moveDuration)
            .SetEase(easeType1)
            .OnComplete(() =>
            {
                // if we still have cuting pints left move knife to next cutting point
                if(nextCuttingPointIndex + 1 < cuttingPoints.Count)
                {
                    knifeTransform.DOLocalMove(knifeTransform.parent.InverseTransformPoint(cuttingPoints[nextCuttingPointIndex + 1]) + (Vector3.up * cuttingPointOffset), moveDuration)
                                .SetEase(easeType2)
                                .OnComplete(() =>
                                {
                                    piecesToCutBodies[nextPieceIndex].sleepThreshold = 0.0f;
                                    piecesToCutBodies[nextPieceIndex].isKinematic = false;
                                    Debug.Log($"Applying explosive at {cuttingPoints[nextCuttingPointIndex]}");
                                    piecesToCutBodies[nextPieceIndex].AddExplosionForce(explosionPower + Random.Range(-.5f, .5f),
                                                                                        cuttingPoints[nextCuttingPointIndex],
                                                                                        explosionRadius,
                                                                                        explosionVerticalOffset);
                                    piecesToCutBodies[nextPieceIndex].AddRelativeForce((piecesToCutBodies[nextPieceIndex].position -
                                                                                        cuttingPoints[nextCuttingPointIndex]).normalized *
                                                                                        moveAwayForce);

                                    nextPieceIndex++;
                                    nextCuttingPointIndex++;
                                    hasCompletedPreviousStep = true;
                                });
                }
                else // else just move it up and run completion method on complete
                {
                    knifeTransform.DOLocalMove(knifeTransform.parent.InverseTransformPoint(cuttingPoints[nextCuttingPointIndex]) + (Vector3.up * cuttingPointOffset), moveDuration)
                                .SetEase(easeType2)
                                .OnComplete(() =>
                                {
                                    for(int i = nextPieceIndex; i < piecesToCutBodies.Length; i++)
                                    {
                                        piecesToCutBodies[nextPieceIndex].sleepThreshold = 0.0f;
                                        piecesToCutBodies[i].isKinematic = false;
                                        //piecesToCutBodies[i].AddExplosionForce(forcePower + Random.Range(-.5f, .5f), cuttingPoints[nextCuttingPointIndex], forceRadius);
                                    }

                                    onCuttingCompleted();
                                });
                }
            });
    }

    private void onCuttingCompleted()
    {
        Debug.Log($"Cutting {gameObject.name} completed!");
        //GetComponentInParent<RecipeScript>().onRecipeStepCompleted();
    }

    private void OnDrawGizmos()
    {
        if(cuttingPoints != null)
        {
            for(int i = 0; i < cuttingPoints.Count; i++)
            {
                Gizmos.color = Color.white;
                Gizmos.DrawLine(cuttingPoints[i] + Vector3.back * .5f, cuttingPoints[i] + Vector3.forward * .5f);
                Gizmos.color = Color.cyan;
                Gizmos.DrawWireSphere(cuttingPoints[nextCuttingPointIndex], explosionRadius);
            }
        }
    }
}
