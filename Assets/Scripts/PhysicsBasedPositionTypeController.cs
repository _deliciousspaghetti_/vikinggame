﻿using System;
using UnityEngine;

public class PhysicsBasedPositionTypeController : MonoBehaviour, IUtencil
{
    public float horizontalInput { get; set; }
    public float verticalInput { get; set; }
    public Action onTapAction { get; set; }
    public bool isDragging { get; set; }

    [SerializeField] private float moveSpeed = 1f;
    [SerializeField] private float minPosition = 5f;
    [SerializeField] private float maxPosition = 15f;
    [SerializeField] private float moveSmoothing = 1f;

    Rigidbody rb;
    private float posY = 0f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if(!isDragging)
            return;

        handleInput();
    }

    public void handleInput()
    {
        posY = transform.position.y;
        posY += verticalInput * (moveSpeed * .1f);
        posY = Mathf.Clamp(posY, minPosition, maxPosition);
        rb.MovePosition(Vector3.Lerp(transform.position, new Vector3(0f, posY, 0f), moveSmoothing));
    }
}
