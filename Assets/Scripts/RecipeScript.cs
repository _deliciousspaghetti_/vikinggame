﻿using System.Collections.Generic;
using UnityEngine;

public class RecipeScript : MonoBehaviour
{
    [SerializeField] private GameObject[] recipeSteps;

    public int stepsCompleted { get; set; }
    public int fryingObjectsCount { get; set; }

    private int requiredSteps;

    private void Awake()
    {
        var allChildren = new List<GameObject>();
        // all these shenanigans just to get child gameobjects as array
        for(int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
            allChildren.Add(transform.GetChild(i).gameObject);
        }
        recipeSteps = allChildren.ToArray();

        requiredSteps = recipeSteps.Length;
    }

    //private void Start()
    //{
    //    stepsCompleted = 0;

    //    recipeSteps[0].SetActive(true);
    //}

    public void onRecipeStepCompleted()
    {
        if(stepsCompleted + 1 < requiredSteps)
        {
            recipeSteps[stepsCompleted].SetActive(false);
            stepsCompleted++;
            recipeSteps[stepsCompleted].SetActive(true);
        }
        else
        {
            Debug.Log("All recipe steps completed.");
        }
    }
}
